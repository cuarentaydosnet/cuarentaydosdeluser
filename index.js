const requestJson = require('request-json');

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    
    var id = "";
    if(event.pathParameters && event.pathParameters.userid) { 
        id = event.pathParameters.userid;
    }  else if (event.queryStringParameters && event.queryStringParameters.id) {
        id = event.queryStringParameters.id;
    } else {  
        var response = { 
            "statusCode": 405,
            "body": "{\"msg\":\"No se puede borrar sin id\"}",
            "isBase64Encoded": false
            };
       return callback(null,response); 
    }
    var query = "user?" + 'q={"_id" : "' + id + '"}'  + "&" + apikey;
    console.log(query);
    var httpClient = requestJson.createClient(db);
    httpClient.get(query, function(err,resMLab,body){
        if (!err && body.length>0) {
            var deleteId =  body[0]._id;
                var deletequery = "user/" + deleteId + "?" + apikey;
                httpClient.del(deletequery,function(err,resMLab,body){
                    if(err){
                        var response = {
                            "statusCode" : 500,
                            "body": JSON.stringify(err)
                        };
                    } else {
                        var response = { 
                            "statusCode": 200,
                            "headers" : {
                                "Access-Control-Allow-Origin": "*"
                            },
                            "body": JSON.stringify(body),
                            "isBase64Encoded": false
                            };
                        }
                    callback(null,response); 
                });
        } else {
            var response = { 
                "statusCode": 404,
                "headers": {
                    "my_header": "my_value"
                },
                "body": '{"msg": "Usuario inexistente"}',
                "isBase64Encoded": false
            };
            callback(null,response);
        }
    });
};

